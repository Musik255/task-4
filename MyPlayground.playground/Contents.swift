import UIKit

var deck = Array(repeating: Array(repeating: "", count: 8), count: 8)

func clearDeck(arrayIn : [[String]]) -> [[String]]{
    var flag = false
    var array = arrayIn
    
    for i in 0...7{
        for j in 0...7{
            if flag == false{
                array[i][j] = "■ "
                flag.toggle()
            }
            else{
                array[i][j] = "□ "
                flag.toggle()
            }
        }
        flag.toggle()
    }
    return array
}


func printDeck(arrayIn : [[String]]){
    let array = arrayIn
    var vertical = 8
    let horizontal = "  a b c d e f g h"
    for i in 0...7{
        print(vertical, terminator: " ")
        vertical -= 1
        for j in 0...7{
            print(array[i][j], terminator: "")
        }
        print()
    }
    print(horizontal)
}

enum Pieces {
    enum Name : String{
        case Pawn = "pawn"
        case King = "king"
        case Queen = "queen"
        case Castle = "castle"
        case Knight = "knight"
        case Bishop = "bishop"
    }
    
    
    enum Side : String{
            case Black = "black"
            case White = "white"
    }
    
    
    enum Living : String{
        case Dead = "dead"
        case Alive = "alive"
    }
    
    
    enum Horizontal : Int{
        case One = 1
        case Two = 2
        case Three = 3
        case Four = 4
        case Five = 5
        case Six = 6
        case Seven = 7
        case Eight = 8

    }
    
    
    enum Vertical : String{
        case A = "A"
        case B = "B"
        case C = "C"
        case D = "D"
        case E = "E"
        case F = "F"
        case G = "G"
        case H = "H"
    }
}


var whiteKing = (Pieces.Name.King, Pieces.Side.White, Pieces.Horizontal.One,
                 Pieces.Living.Alive, Pieces.Vertical.A)

var blackKing = (Pieces.Name.King, Pieces.Side.Black, Pieces.Horizontal.Three,
                 Pieces.Living.Alive, Pieces.Vertical.A)

var blackCastle = (Pieces.Name.Castle, Pieces.Side.Black, Pieces.Horizontal.One,
                   Pieces.Living.Alive, Pieces.Vertical.H)
    
var allPieces = [whiteKing, blackKing, blackCastle]

//var triple = ("king", "b", 2)

//func move (triple : (name : String, vert : String, hor :Int)){
//    switch triple.vert{
//    case "a":
//        whiteKing.4 = .A
//
//    case "b":
//        whiteKing.4 = .B
//
//    case "c":
//        whiteKing.4 = .C
//
//    case "d":
//        whiteKing.4 = .D
//
//    case "e":
//        whiteKing.4 = .E
//
//    case "f":
//        whiteKing.4 = .F
//
//    case "g":
//        whiteKing.4 = .G
//
//    case "h":
//        whiteKing.4 = .H
//    default:
//    print()
//    }
//
//
//}

//whiteKing.0 = .Pawn
//
//print(whiteKing.0)

//for i in allPieces{
//    print(i.0)
//}


func arrangementPieces(array : [(Pieces.Name,
                                 Pieces.Side,
                                 Pieces.Horizontal,
                                 Pieces.Living,
                                 Pieces.Vertical)],
                       arrayIn : [[String]]) -> [[String]]{
    var arrayDeck = arrayIn
    for i in array{
        
        var vertical = 0
        var horizontal = 0
        var piece = ""
        
        switch i.2{
        case .One:
            horizontal = 0
        case .Two:
            horizontal = 1
        case .Three:
            horizontal = 2
        case .Four:
            horizontal = 3
        case .Five:
            horizontal = 4
        case .Six:
            horizontal = 5
        case .Seven:
            horizontal = 6
        case .Eight:
            horizontal = 7
        }
        
        switch i.4{
        case .A:
            vertical = 0
        case .B:
            vertical = 1
        case .C:
            vertical = 2
        case .D:
            vertical = 3
        case .E:
            vertical = 4
        case .F:
            vertical = 5
        case .G:
            vertical = 6
        case .H:
            vertical = 7
        }
        switch i.0{
            
        case .Pawn:
            switch i.1{
                
            case .Black:
                piece = "♙ "
            case .White:
                piece = "♟ "
            }
        case .King:
            switch i.1{
                
            case .Black:
                piece = "♔ "
            case .White:
                piece = "♚ "
            }
        case .Queen:
            switch i.1{
                
            case .Black:
                piece = "♕ "
            case .White:
                piece = "♛ "
            }
        case .Castle:
            switch i.1{
                
            case .Black:
                piece = "♖ "
            case .White:
                piece = "♜ "
            }
        case .Knight:
            switch i.1{
                
            case .Black:
                piece = "♘ "
            case .White:
                piece = "♞ "
            }
        case .Bishop:
            switch i.1{
                
            case .Black:
                piece = "♗ "
            case .White:
                piece = "♝ "
            }
        }
        arrayDeck[7-horizontal][vertical] = piece
    }
    return arrayDeck
}

func printInfo(array : [(Pieces.Name,
                         Pieces.Side,
                         Pieces.Horizontal,
                         Pieces.Living,
                         Pieces.Vertical)])
{
    for i in array{
        print("Фигура: \(i.0.rawValue) \nЦвет: \(i.1.rawValue) \nГоризонталь: \(i.2.rawValue) \nВертикаль: \(i.4.rawValue) \nЖизнь: \(i.3.rawValue)")
        print()
    }
}


//Ставим на доску фигуры

deck = arrangementPieces(array: allPieces, arrayIn: deck)

//Печатаем фигуры
printDeck(arrayIn: deck)
